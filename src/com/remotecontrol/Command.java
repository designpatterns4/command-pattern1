package com.remotecontrol;

public interface Command {
    public void execute();
    public void undo();

}
