package com.remotecontrol;

public class RemoteControlWithUndo {
    Command[] onCommands;
    Command[] offCommands;
    Command undoCommand;

    public RemoteControlWithUndo() {
        this.onCommands = new Command[7];
        this.offCommands = new Command[7];

        Command noCommand = new NoCommand();
        for (int i=0; i < 7 ; i++) {
            onCommands[i] = noCommand;
            offCommands[i]= noCommand;
        }
        undoCommand = noCommand;
    }
    public void setCommand(int slot, Command onCommand, Command offCommand) {
        onCommands[slot]=onCommand;
        offCommands[slot]=offCommand;
    }

    public void onButtonWasPushed(int slot){
        onCommands[slot].execute();
        undoCommand = onCommands[slot];
    }

    public void offButtonWasPushed(int slot){
        offCommands[slot].execute();
        undoCommand = offCommands[slot];
    }

    public void undoButtonWasPushed() {
        undoCommand.undo();
    }

    public String toString() {
        StringBuilder stringbuff = new StringBuilder();
        stringbuff.append("\n------ Remote Control ------\n");
        for (int i=0; i < onCommands.length ; i++) {
            stringbuff.append("[slot ").append(i).append("] ").append(onCommands[i].getClass().getName()).append("   ").append(offCommands[i].getClass().getName()).append("\n");
        }
        stringbuff.append("[undo] ").append(undoCommand.getClass().getName()).append("\n");
        return stringbuff.toString();
    }
}
